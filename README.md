Formation à l'usage de R + quarto pour la recherche reproductible en SHS.


# Licence

Formation Quarto © 2023 by Elias Bouacida is licensed under CC BY-SA 4.0. To view a copy of this license, visit <http://creativecommons.org/licenses/by-sa/4.0/>